package com.dhv.mydemo1;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dhv.mydemo1.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etEmail, etContact;
    Button btnSubmit;
    ImageView ivBackground;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    CheckBox cbGames, cbReading, cbJourney;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        etEmail = findViewById(R.id.etActEmail);
        etContact = findViewById(R.id.etActContact);
        btnSubmit = findViewById(R.id.btnActSubmit);
        ivBackground = findViewById(R.id.ivActBackground);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        cbGames = findViewById(R.id.cbActGames);
        cbReading = findViewById(R.id.cbActReading);
        cbJourney = findViewById(R.id.cbActJourney);



        // Background method :


// Onclick method :
        btnSubmit.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                HashMap<String, Object> map = new HashMap<>();
                map.put(Const.First_name, etFirstName.getText().toString());
                map.put(Const.Last_name, etLastName.getText().toString());
                map.put(Const.Email_id, etEmail.getText().toString());
                map.put(Const.Phone_no, etContact.getText().toString());


                String gender = "";
                if (rbMale.isChecked()) {
                    gender = "M";

                } else if (rbFemale.isChecked()) {
                    gender = "F";
                }
                map.put(Const.Gender, gender);

                String hobbies = "";
                if (cbGames.isChecked()) {
                    hobbies += "," + cbGames.getText().toString();
                }
                if (cbJourney.isChecked()) {
                    hobbies += "," + cbJourney.getText().toString();
                }
                if (cbReading.isChecked()) {
                    hobbies += "," + cbReading.getText().toString();
                }
                if (hobbies.length() > 0) {
                    hobbies.substring(1);
                }
                map.put(Const.Hobby, hobbies);
                userList.add(map);

                Log.d("MainActivityLog:::::", "" + userList);

                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("UserList", userList);
                startActivity(intent);


                // ShowToast method
                       /*showToast();

                        String firstName = etFirstName.getText().toString();
                        String lastName = etLastName.getText().toString();
                        String concat = firstName + " " + lastName;
                        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                        intent.putExtra("concat", concat);
                        startActivity(intent);*/

                //Toast :
                /*Toast.makeText(getApplicationContext(),rbMale.isChecked()?"Male":"Female",Toast.LENGTH_LONG).show();*/

                //HashMap :
                       /* HashMap<Integer,Object> ar=new HashMap<>();
                        ar.put(5,"Dhruv");
                        ar.put(6,"Gadhiya");
                        Log.d(":1:",""+ar.get(5));*/

                //ArrayList :
                       /* ArrayList<String> al=new ArrayList<>();
                        al.add("Android");
                        al.add("Wokshop");
                        al.add(1,"DIET");
                        Log.d(":List:",""+al);*/


            }

        });
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");
        btnSubmit.setTypeface(typeface);
    }

    // ShowToast :
    public void showToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_main, (ViewGroup) findViewById(R.id.llToast));
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 200);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    }
