package com.dhv.mydemo1.util;

public class Const {
    public static final String First_name = "firstName";
    public static final String Last_name = "lastName";
    public static final String Gender = "gender";
    public static final String Phone_no = "phoneNo";
    public static final String Email_id = "emailId";
    public static final String Hobby = "hobby";
}
